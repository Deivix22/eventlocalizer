package admateo.eventlocalizer;

import android.view.View;

/**
 * Created by David on 18/10/2016.
 */

public interface RecyclerViewOnItemClickListener {
    void onClick(View v, int position);
}
