package admateo.eventlocalizer;

/**
 * Created by David on 17/01/2017.
 */

public class EventoItem {
    //Declaramos las variables
    private String id, nombre, inicio;

    /**
     * Constructor de clase
     * @param id
     * @param nombre
     * @param inicio
     */
    public EventoItem(String id, String nombre, String inicio) {
        this.id = id;
        this.nombre = nombre;
        this.inicio = inicio;
    }

    /**
     * Metodo get Id
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Metodo set Id
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Metodo get Nombre
     * @return nombre
     */
    public String getNombre() {

        return nombre;
    }

    /**
     *  Metodo set Nombre
     * @param nombre
     */
    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    /**
     *  Metodo get Inicio
     * @return inicio
     */
    public String getInicio() {
        return inicio;
    }

    /**
     *  Metodo set Inicio
     * @param inicio
     */
    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

}
