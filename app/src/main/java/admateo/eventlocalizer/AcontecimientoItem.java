package admateo.eventlocalizer;

/**
 * Created by David on 18/10/2016.
 */

public class AcontecimientoItem {

    //Declaramos las variables
    private String id;
    private String nombre;
    private String fecha_inicio;
    private String fecha_final;

    /**
     * Constructor de la clase
     * @param id
     * @param nombre
     * @param fecha_inicio
     * @param fecha_final
     */
    public AcontecimientoItem(String id, String nombre, String fecha_inicio, String fecha_final) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_inicio = fecha_inicio;
        this.fecha_final = fecha_final;
    }

    /**
     * Metodo get ID
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Metodo set id
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *  Metodo get nombre
     * @return nombre
     */
    public String getNombre() {

        return nombre;
    }

    /**
     *  Metodo set nombre
     * @param nombre
     */
    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    /**
     *  Metodo get fecha_inicio
     * @return fecha_inicio
     */
    public String getFecha_inicio() {
        return fecha_inicio;
    }

    /**
     * Metodo set fecha_inicio
     * @param fecha_inicio
     */
    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    /**
     *  Metodo get fecha_final
     * @return fecha_final
     */
    public String getFecha_final() {
        return fecha_final;
    }

    /**
     * Metodo set fecha_final
     * @param fecha_final
     */
    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }
}