package admateo.eventlocalizer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by David on 17/01/2017.
 */

public class EventoAdapter extends ArrayAdapter<EventoItem> {

    //Declaramos variables
    private static String TAG = "EventosAdapter";
    private final Context context;
    private final ArrayList<EventoItem> items;

    /**
     * Constructor de la clase
     * @param context
     * @param resource
     * @param items
     */
    public EventoAdapter(Context context, int resource, ArrayList<EventoItem> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }

    /**
     * Personaliza la vista de cada elemento de la lista
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Mylog.d(TAG, "getView Evento Adapter");

        // Carga en la vista el layout de la fila de la lista
        // y le asigna los datos
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.evento_row, parent, false);

        TextView textView_nombre = (TextView) rowView.findViewById(R.id.evento_nombre);
        textView_nombre.setText(items.get(position).getNombre());

        TextView textView_inicio = (TextView) rowView.findViewById(R.id.evento_inicio);
        textView_inicio.setText(items.get(position).getInicio());

        return rowView;
    }

}
