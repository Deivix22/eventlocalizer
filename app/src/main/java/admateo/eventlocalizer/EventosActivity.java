package admateo.eventlocalizer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by David on 17/01/2017.
 */

public class EventosActivity extends AppCompatActivity implements ListadoEventosFragment.OnListadoFragmentListener {

    private static String TAG = "EventosActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mylog.d(TAG, "onCreate EventosActivity");
        setContentView(R.layout.activity_eventos);

        // Comprueba qué layout se ha cargado:
        // - Si es "unique_fragment" sólo hay un fragmento en el layout
        //   y lo tenemos que añadir nosotros al "FrameLayout".
        // - Si no, los dos fragmentos conviven en la actividad
        //   y se añaden automáticamente desde el layout al "fragment".
        if (findViewById(R.id.unique_fragment) != null) {

            // Sólo crea el fragmento "listado" si no se ha restaurado
            // de un estado previo de la actividad
            // para evitar que se solapen múltiples instancias
            if (savedInstanceState == null) {
                ListadoEventosFragment listadoFragment = new ListadoEventosFragment();

                // Recupera los datos extras que posea la actividad
                // y se los pasa al fragmento
                listadoFragment.setArguments(getIntent().getExtras());

                // Añade el fragmento "listado" al "FrameLayout"
                getSupportFragmentManager().beginTransaction().add(R.id.unique_fragment, listadoFragment).commit();
            }

        }
    }

    public void onListadoInteraction(int position, EventoItem item) {
        Mylog.d(TAG, "onListadoInteraction EventosActivity");

        // Captura el fragmento "contenido" para ver si esta cargado en la actividad
        // - Si existe, actualizamos sus datos, porque tendremos los dos fragmentos cargados.
        // - Si no existe, lo creamos y lo cargamos en el "FrameLayout" sustituyendo la lista.
        MostrarEventoFragment contenidoFrag = (MostrarEventoFragment) getSupportFragmentManager().findFragmentById(R.id.mostrar_fragment);

        if (contenidoFrag != null) {
            contenidoFrag.updateContenidoFragment(item.getId());
        } else {
            // Crea el fragmento y le pasa datos extras
            MostrarEventoFragment newContenidoFragment = new MostrarEventoFragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            args.putString("id", item.getId());
            newContenidoFragment.setArguments(args);


            // Reemplaza el fragmento "listado" por el fragmento "contenido"
            // Añade la transacción a la pila para poder volver atrás
            // Al volver atrás se detiene el fragmento pero no se destruye
            getSupportFragmentManager().beginTransaction().replace(R.id.unique_fragment, newContenidoFragment).addToBackStack(null).commit();
        }

    }

}
