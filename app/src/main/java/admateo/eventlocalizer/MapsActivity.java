package admateo.eventlocalizer;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by David on 13/10/2016.
 */

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //Declaramos las variables
    private GoogleMap mMap;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        SharedPreferences prefs = getSharedPreferences("prefenciasID", Context.MODE_PRIVATE);
        id = prefs.getString("id_acontecimiento", "0");

        //Creamos la conexion y leemos de la bbdd
        AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(this, Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
        SQLiteDatabase db = AconSQLite.getReadableDatabase();

        //Creamos el cursor y hacemos la consulta select que nos devolvera el acontecimiento
        Cursor c = db.rawQuery("SELECT nombre, latitud, longitud FROM acontecimiento WHERE id='" + id + "'", null);

        if (c.moveToFirst()) {
            // Nos creamos el objeto LatLng y le pasamos la latitud y la longitud
            LatLng acontecimiento = new LatLng(c.getDouble(c.getColumnIndex("latitud")), c.getDouble(c.getColumnIndex("longitud")));
            //Le ponemos al marcador el nombre del acontecimiento
            mMap.addMarker(new MarkerOptions().position(acontecimiento).title(c.getString(c.getColumnIndex("nombre"))).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN )));
            //Con el metodo moveCamera, le pasamos un objeto de tipo newLatLngZoom y le pasamos el objeto LatLng y el zoom que queremos
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(acontecimiento, (float) 13.7));
        }
        c.close();


        //Creamos el cursor y hacemos la consulta select que nos devolvera el acontecimiento
        Cursor c1 = db.rawQuery("SELECT nombre, latitud, longitud FROM eventos WHERE id_acontecimientos='" + id + "'", null);

        if (c1.moveToFirst()) {
            do{
                // Nos creamos el objeto LatLng y le pasamos la latitud y la longitud
                LatLng evento = new LatLng(c1.getDouble(c1.getColumnIndex("latitud")), c1.getDouble(c1.getColumnIndex("longitud")));
                //Le ponemos al marcador el nombre del acontecimiento
                mMap.addMarker(new MarkerOptions().position(evento).title(c1.getString(c1.getColumnIndex("nombre"))).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            }while(c1.moveToNext());

        }
        c1.close();


    }
}
