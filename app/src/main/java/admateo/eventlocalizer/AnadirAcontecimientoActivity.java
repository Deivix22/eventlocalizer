package admateo.eventlocalizer;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by David on 08/11/2016.
 */

public class AnadirAcontecimientoActivity extends AppCompatActivity {

    //Declaramos las variables
    final private String ACTIVITY = "AnadirAcontecimientoActivity";
    final private int REQUEST_CODE_INTERNET = 10;
    Context context;
    EditText editTextID;
    Button button_AñadirID;
    ProgressBar progressBar;

    /**
     * Recoge el id que se introduce en el editText y se lo pasa a la clase AcontecimientoAsyncTask, ademas de la vista, el contexto y la progressBar, ademas comprueba si tenemos los permisos de escritura y de internet
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_acontecimiento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Recoge los id del editText, button y progressBar
        editTextID = (EditText) findViewById(R.id.editText_id);
        button_AñadirID = (Button) findViewById(R.id.button_añadirID);
        progressBar = (ProgressBar) findViewById(R.id.progressBarAñadirAcontecimiento);

        //Recoge el contexto
        context = this;

        //Cuando se pulse el boton recojera lo que hemos escrito en el editText y lo se lo pasa a la clase AcontecimientoAsynkTask, ademas de comprobar si tenemos los permisos de escritura e internet
        button_AñadirID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoID = editTextID.getText().toString();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextID.getWindowToken(), 0);

                if (textoID.isEmpty()) {
                    Snackbar.make(v, R.string.SnackbarID, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    Mylog.d("build", " " + Build.VERSION.SDK_INT);
                    // Comprobar permiso
                    int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET);

                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        new AcontecimientoAsyncTask(context, v, textoID, progressBar).execute();
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+
                            // Explicar permiso
                            if (ActivityCompat.shouldShowRequestPermissionRationale(AnadirAcontecimientoActivity.this, Manifest.permission.INTERNET)) {
                                Toast.makeText(context, "El permiso es necesario para utilizar internet.", Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            ActivityCompat.requestPermissions(AnadirAcontecimientoActivity.this, new String[]{Manifest.permission.INTERNET}, REQUEST_CODE_INTERNET);

                        } else {
                            // Pre-Marshmallow
                            Snackbar.make(v, "No tienes permisos de internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }


                    }
                }
            }
        });
    }

    /**
     * Logs
     */
    @Override
    protected void onStart() {
        Mylog.d(ACTIVITY, "onStart...");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Mylog.d(ACTIVITY, "onResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Mylog.d(ACTIVITY, "onPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Mylog.d(ACTIVITY, "onStop...");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Mylog.d(ACTIVITY, "onRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Mylog.d(ACTIVITY, "onDestroy...");
        super.onDestroy();
    }
}
