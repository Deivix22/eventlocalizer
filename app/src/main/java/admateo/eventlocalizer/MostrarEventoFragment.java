package admateo.eventlocalizer;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by David on 13/10/2016.
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class MostrarEventoFragment extends Fragment {

    private static String TAG = "MostrarEventoFragment";
    private String id = "";
    private String id_acontecimientos;
    private String nombre;
    private String descripcion;
    private String inicio;
    private String fin;
    private String direccion;
    private String localidad;
    private String cod_postal;
    private String provincia;
    private String longitud;
    private String latitud;

    /**
     * El constructor público vacío es obligatorio
     */
    public MostrarEventoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Mylog.d(TAG, "onCreateView...");
        super.onCreateView(inflater, container, savedInstanceState);

        // Si la actividad se recrea (por ejemplo después de una rotación)
        // recupera los datos de estado guardados por "onSaveInstanceState".
        // Necesario cuando los fragmentos "listado" y "contenido" están visibles
        if (savedInstanceState != null) {
            id = savedInstanceState.getString("id");
            //id_acontecimiento = savedInstanceState.getString("id_acontecimiento");
        }

        return inflater.inflate(R.layout.fragment_mostrar_evento, container, false);
    }

    @Override
    public void onStart() {
        Mylog.d(TAG, "onStart");
        super.onStart();

        // Comprueba si se han pasado datos extras al fragmento
        // y se actualiza la vista en función de ellos
        Bundle args = getArguments();
        if (args != null) {
            // Actualiza el contenido en función de los datos extras recibidos
            updateContenidoFragment(args.getString("id"));
            //updateContenidoFragment(args.getString("id_acontecimiento"));
        } else if (!id.equals("")) {
            // Set article based on saved instance state defined during onCreateView
            // Actualiza el contenido en función de los datos de estado guardados
            updateContenidoFragment(id);
        }
    }

    /**
     * Actualiza los datos del fragmento
     * Aqui tenemos que crear los textview dinamicos
     */
    public void updateContenidoFragment(String id) {
        Mylog.d(TAG, "updateContenidoFragment");

        //Creamos la conexion y leemos de la bbdd
        AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(getContext(), Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
        SQLiteDatabase db = AconSQLite.getReadableDatabase();

        //Creamos el cursor y hacemos la consulta select que nos devolvera el acontecimiento
        Cursor c = db.rawQuery("SELECT * FROM eventos WHERE id='" + id + "'", null);

        if (c.moveToFirst()) {
            LinearLayout linearLayoutPrincipal = (LinearLayout) getActivity().findViewById(R.id.fragment_mostrar_evento);
            //Elimina todas las vistas en el layout de mostrar evento para que no se acumulen
            linearLayoutPrincipal.removeAllViewsInLayout();
            do {
                nombre = c.getString(c.getColumnIndex("nombre"));
                id_acontecimientos = c.getString(c.getColumnIndex("id_acontecimientos"));
                descripcion = c.getString(c.getColumnIndex("descripcion"));
                inicio = c.getString(c.getColumnIndex("inicio"));
                fin = c.getString(c.getColumnIndex("fin"));
                direccion = c.getString(c.getColumnIndex("direccion"));
                localidad = c.getString(c.getColumnIndex("localidad"));
                cod_postal = c.getString(c.getColumnIndex("cod_postal"));
                provincia = c.getString(c.getColumnIndex("provincia"));
                longitud = c.getString(c.getColumnIndex("longitud"));
                latitud = c.getString(c.getColumnIndex("latitud"));

                //Creamos los formatos de entrada y de salida de las fechas
                SimpleDateFormat formatoDeEntrada = new SimpleDateFormat("yyyyMMddHHmm");
                SimpleDateFormat formatoDeSalida = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                //Creamos las dos variables de tipo date y las inicializamos a null porque deben inicializarse
                Date dateInicio = null;
                Date dateFin = null;

                try {
                    dateInicio = formatoDeEntrada.parse("" + inicio + "");
                    dateFin = formatoDeEntrada.parse("" + fin + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String inicioFormateado = formatoDeSalida.format(dateInicio);
                String finFormateado = formatoDeSalida.format(dateFin);


                if (!nombre.isEmpty()) {
                    añadirDatosLayout(nombre, R.drawable.ic_assignment_ind_black_24dp, linearLayoutPrincipal);
                }
                if (!descripcion.isEmpty()) {
                    añadirDatosLayout(descripcion, R.drawable.ic_event_seat_black_24dp, linearLayoutPrincipal);
                }
                if (!inicio.isEmpty()) {
                    añadirDatosLayout(inicioFormateado, R.drawable.ic_alarm_black_24dp, linearLayoutPrincipal);
                }
                if (!fin.isEmpty()) {
                    añadirDatosLayout(finFormateado, R.drawable.ic_alarm_on_black_24dp, linearLayoutPrincipal);
                }
                if (!direccion.isEmpty()) {
                    añadirDatosLayout(direccion, R.drawable.ic_navigation_black_24dp, linearLayoutPrincipal);
                }
                if (!localidad.isEmpty()) {
                    añadirDatosLayout(localidad, R.drawable.ic_room_black_24dp, linearLayoutPrincipal);
                }
                if (!cod_postal.isEmpty()) {
                    añadirDatosLayout(cod_postal, R.drawable.ic_local_parking_black_24dp, linearLayoutPrincipal);
                }
                if (!provincia.isEmpty()) {
                    añadirDatosLayout(provincia, R.drawable.ic_place_black_24dp, linearLayoutPrincipal);
                }
                if (!longitud.isEmpty()) {
                    añadirDatosLayout(longitud, R.drawable.ic_my_location_black_24dp, linearLayoutPrincipal);
                }
                if (!latitud.isEmpty()) {
                    añadirDatosLayout(latitud, R.drawable.ic_my_location_black_24dp, linearLayoutPrincipal);
                }

            } while (c.moveToNext());

        }

        c.close();
    }

    /**
     * Almacena el estado del fragmento para poder ser recuperado
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Mylog.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        // Guarda los datos
        outState.putString("id", id);
    }


    protected void añadirDatosLayout(String nombre, int rutaImagen, LinearLayout layoutparams) {
        //Crea un segundo layout en el que introduciremos los campos
        LinearLayout milayout = new LinearLayout(new ContextThemeWrapper(getContext(), R.style.AppTheme));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        milayout.setLayoutParams(params);
        milayout.setOrientation(LinearLayout.HORIZONTAL);

        //Creamos un nuevo objeto ImageView para asignarle un icono
        ImageView imagen = new ImageView(new ContextThemeWrapper(getContext(), R.style.AppTheme));
        imagen.setImageResource(rutaImagen);

        //Creamos un nuevo objeto TextView para pasarle cada dato obtenido de la consulta
        TextView campo = new TextView(new ContextThemeWrapper(getContext(), R.style.AppTheme));
        campo.setText(nombre);

        //Añadimos el layoutParams al TextView y al ImageView
        imagen.setLayoutParams(params);
        campo.setLayoutParams(params);

        //Añadimos los dos objetos al LinearLayout
        milayout.addView(imagen);
        milayout.addView(campo);

        //Se añade nuestro layout secundario al principal
        layoutparams.addView(milayout);

    }


}
