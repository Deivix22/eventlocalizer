package admateo.eventlocalizer;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by David on 17/01/2017.
 */

public class ListadoEventosFragment extends ListFragment {

    private static String TAG = "ListadoFragment";
    private ArrayList<EventoItem> eventoItems;
    private ListView listView;
    private String id, inicio;
    private OnListadoFragmentListener myListener;

    public ListadoEventosFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mylog.d(TAG, "onCreate...");
        // Recupera los datos extras que nos envíe la actividad
        if (getArguments() != null) {
            //myParam = getArguments().getString(ARG_PARAM);
        }

    }

    /**
     * OnCreateView crea las vistas del fragmento
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Mylog.d(TAG, "onCreateView...");

        // Carga el layout del fragmento y recupera el ListView para modificarlo posteriormente
        View rootView = inflater.inflate(R.layout.fragment_listado_eventos, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        return rootView;
    }

    /**
     * OnActivityCreated se llama cuando OnCreate de la actividad ha terminado
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Mylog.d(TAG, "onActivityCreated...");

        SharedPreferences prefs = getContext().getSharedPreferences("prefenciasID", Context.MODE_PRIVATE);
        id = prefs.getString("id_acontecimiento", "0");

        AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(getContext(), Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
        SQLiteDatabase db = AconSQLite.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT id, nombre, inicio FROM eventos where id_acontecimientos=" + id + " ORDER BY inicio asc", null);

        if (c.moveToFirst()) {
            eventoItems = new ArrayList<EventoItem>();

            SimpleDateFormat formatoDeEntrada = new SimpleDateFormat("yyyyMMddHHmm");
            SimpleDateFormat formatoDeSalida = new SimpleDateFormat("dd/MM/yyyy - HH:mm");

            //Creamos las dos variables de tipo date y las inicializamos a null porque deben inicializarse
            Date dateInicio = null;

            do {
                try {
                    dateInicio = formatoDeEntrada.parse("" + c.getString(c.getColumnIndex("inicio")) + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                inicio = formatoDeSalida.format(dateInicio);

                // Crea los elementos a mostrar en la lista
                eventoItems.add(new EventoItem(c.getString(c.getColumnIndex("id")), c.getString(c.getColumnIndex("nombre")), inicio));

            } while (c.moveToNext());
            // Crea el adaptador con los elementos y se lo asigna al ListView
            // Utiliza el layout predefinido de Android para la lista
            EventoAdapter eventosAdapter = new EventoAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, eventoItems);
            listView.setAdapter(eventosAdapter);

            // Si están cargados los fragmentos "listado" y "contenido" al mismo tiempo
            // configura la lista para que sólo se pueda elegir un elemento
            if (getFragmentManager().findFragmentById(R.id.mostrar_fragment) != null) {
                getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Mylog.d(TAG, "onListItemClick");
        // Notifica a la actividad el elemento seleccionado
        if (myListener != null) {
            myListener.onListadoInteraction(position, eventoItems.get(position));
        }
        // Marca como activo el elemento seleccionado
        // En este caso no funciona al utilizar un adaptador personalizado
        getListView().setItemChecked(position, true);
    }

    /**
     * Se ejecuta cuando se asocia el fragmento a la actividad
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Mylog.d(TAG, "onAttach");

        // Comprueba si está implementado el interfaz en la actividad
        // y se lo asigna
        if (context instanceof OnListadoFragmentListener) {
            myListener = (OnListadoFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListadoFragmentListener");
        }
    }

    /**
     * Se ejecuta cuando se desasocia el fragmento de la actividad
     */
    @Override
    public void onDetach() {
        super.onDetach();
        Mylog.d(TAG, "onDetach...");

        // Elimina el interfaz de la actividad
        myListener = null;
    }

    /**
     * Interfaz que debe definir en su declaración la actividad
     */
    public interface OnListadoFragmentListener {
        void onListadoInteraction(int position, EventoItem item);
    }
}
