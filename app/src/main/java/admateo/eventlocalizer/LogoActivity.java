package admateo.eventlocalizer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by David on 13/10/2016.
 */

public class LogoActivity extends AppCompatActivity {

    private static final String ACTIVITY = "LogoActivity";
    Boolean opcion;
    String idioma;

    /**
     * Muestra el icono de nuestra aplicacion durante 3 segundos y luego pasa a la actividad ListadoAcontecimientosActivity
     *
     * @param savedInstanceState
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //ocultar la barra de arriba (barra de notificaciones)
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE); //Obtenemos el windowManager para poder trabajar con el
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //Ocultamos la barra

        //ocultar la barra de abajo (barra de navegacion)
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        // Crea el objeto SharedPreferences para recuperar las opciones de SettingsActivity
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        // Recupera la opcion marcada en SettingActivity (mostrar el ultimo acontecimiento visitado)
        opcion = pref.getBoolean("UltimoAcon", false);


        // Recupera la opcion del idioma
        idioma = pref.getString("Idioma", "Español");

        //Lo cambia en funcion de la opcion seleccionada en la pestaña configuracion
        Locale localizacion = new Locale(idioma, "ES");

        Locale.setDefault(localizacion);
        Configuration config = new Configuration();
        config.locale = localizacion;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());


        //Se mostrara la pantalla durante 3 segundos y luego pasara a la pantalla ListadoAcontecimientosActivity
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), ListadoAcontecimientosActivity.class));
                if (opcion) {//Si la opcion recuperada es true, se ira directamente a MostrarAcontecimientoActivity
                    startActivity(new Intent(getApplicationContext(), MostrarAcontecimientoActivity.class));
                }
                finish();

            }
        }, 3000);
    }

    /**
     * Logs
     */
    @Override
    protected void onStart() {
        Mylog.d(ACTIVITY, "onStart...");
        super.onStart();

    }

    @Override
    protected void onResume() {
        Mylog.d(ACTIVITY, "onResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Mylog.d(ACTIVITY, "onPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Mylog.d(ACTIVITY, "onStop...");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Mylog.d(ACTIVITY, "onRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Mylog.d(ACTIVITY, "onDestroy...");
        super.onDestroy();
    }
    //Termina el log
}
