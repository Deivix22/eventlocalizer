package admateo.eventlocalizer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by David on 13/10/2016.
 */

public class MostrarAcontecimientoActivity extends AppCompatActivity {

    //Declaramos las variables
    private static final String ACTIVITY = "StartCreate";
    String id, nombre, organizador, descripcion, tipo, portada, inicio, fin, direccion, localidad, cod_postal, provincia, longitud, latitud, telefono, email, web, facebook, twitter, instagram;
    private int numColumnEvento;
    private Button mapa;

    /**
     * Lee de la bbdd el acontecimiento descargado y lo muestra en la actividad, ademas comprueba que campos estan vacios o no para no mostrarlos
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_acontecimiento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Recogemos el id del fichero XML
        SharedPreferences prefs = getSharedPreferences("prefenciasID", Context.MODE_PRIVATE);
        id = prefs.getString("id_acontecimiento", "0");

        //Creamos la conexion y leemos de la bbdd
        AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(this, Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
        SQLiteDatabase db = AconSQLite.getReadableDatabase();

        //Creamos el cursor y hacemos la consulta select que nos devolvera el acontecimiento
        Cursor c = db.rawQuery("SELECT * FROM acontecimiento WHERE id='" + id + "'", null);

        LinearLayout linearLayoutPrincipal = (LinearLayout) findViewById(R.id.linearLayoutMostrarAcontecimiento);

        if (c.moveToFirst()) {

            linearLayoutPrincipal.setOrientation(LinearLayout.VERTICAL);
            do {
                nombre = c.getString(c.getColumnIndex("nombre"));
                organizador = c.getString(c.getColumnIndex("organizador"));
                descripcion = c.getString(c.getColumnIndex("descripcion"));
                tipo = c.getString(c.getColumnIndex("tipo"));
                portada = c.getString(c.getColumnIndex("portada"));
                inicio = c.getString(c.getColumnIndex("inicio"));
                fin = c.getString(c.getColumnIndex("fin"));
                direccion = c.getString(c.getColumnIndex("direccion"));
                localidad = c.getString(c.getColumnIndex("localidad"));
                cod_postal = c.getString(c.getColumnIndex("cod_postal"));
                provincia = c.getString(c.getColumnIndex("provincia"));
                longitud = c.getString(c.getColumnIndex("longitud"));
                latitud = c.getString(c.getColumnIndex("latitud"));
                telefono = c.getString(c.getColumnIndex("telefono"));
                email = c.getString(c.getColumnIndex("email"));
                web = c.getString(c.getColumnIndex("web"));
                facebook = c.getString(c.getColumnIndex("facebook"));
                twitter = c.getString(c.getColumnIndex("twitter"));
                instagram = c.getString(c.getColumnIndex("instagram"));


                //Creamos los formatos de entrada y de salida de las fechas
                SimpleDateFormat formatoDeEntrada = new SimpleDateFormat("yyyyMMddHHmm");
                SimpleDateFormat formatoDeSalida = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                //Creamos las dos variables de tipo date y las inicializamos a null porque deben inicializarse
                Date dateInicio = null;
                Date dateFin = null;

                try {
                    dateInicio = formatoDeEntrada.parse("" + inicio + "");
                    dateFin = formatoDeEntrada.parse("" + fin + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String inicioFormateado = formatoDeSalida.format(dateInicio);
                String finFormateado = formatoDeSalida.format(dateFin);


                if (!nombre.isEmpty()) {
                    añadirDatosLayout(nombre, R.drawable.ic_assignment_ind_black_24dp, linearLayoutPrincipal);
                }
                if (!organizador.isEmpty()) {
                    añadirDatosLayout(organizador, R.drawable.ic_face_black_24dp, linearLayoutPrincipal);
                }
                if (!descripcion.isEmpty()) {
                    añadirDatosLayout(descripcion, R.drawable.ic_event_seat_black_24dp, linearLayoutPrincipal);
                }
                if (!tipo.isEmpty()) {
                    añadirDatosLayout(tipo, R.drawable.ic_event_black_24dp, linearLayoutPrincipal);
                }
                if (!portada.isEmpty()) {
                    añadirDatosLayout(portada, R.drawable.ic_supervisor_account_black_24dp, linearLayoutPrincipal);
                }
                if (!inicio.isEmpty()) {
                    añadirDatosLayout(inicioFormateado, R.drawable.ic_alarm_black_24dp, linearLayoutPrincipal);
                }
                if (!fin.isEmpty()) {
                    añadirDatosLayout(finFormateado, R.drawable.ic_alarm_on_black_24dp, linearLayoutPrincipal);
                }
                if (!direccion.isEmpty()) {
                    añadirDatosLayout(direccion, R.drawable.ic_navigation_black_24dp, linearLayoutPrincipal);
                }
                if (!localidad.isEmpty()) {
                    añadirDatosLayout(localidad, R.drawable.ic_room_black_24dp, linearLayoutPrincipal);
                }
                if (!cod_postal.isEmpty()) {
                    añadirDatosLayout(cod_postal, R.drawable.ic_local_parking_black_24dp, linearLayoutPrincipal);
                }
                if (!provincia.isEmpty()) {
                    añadirDatosLayout(provincia, R.drawable.ic_place_black_24dp, linearLayoutPrincipal);
                }
                if (!longitud.isEmpty()) {
                    añadirDatosLayout(longitud, R.drawable.ic_my_location_black_24dp, linearLayoutPrincipal);
                }
                if (!latitud.isEmpty()) {
                    añadirDatosLayout(latitud, R.drawable.ic_my_location_black_24dp, linearLayoutPrincipal);
                }
                if (!telefono.isEmpty()) {
                    añadirDatosLayout(telefono, R.drawable.ic_local_phone_black_24dp, linearLayoutPrincipal);
                }
                if (!email.isEmpty()) {
                    añadirDatosLayout(email, R.drawable.message_48, linearLayoutPrincipal);
                }
                if (!web.isEmpty()) {
                    añadirDatosLayout(web, R.drawable.web_search, linearLayoutPrincipal);
                }
                if (!facebook.isEmpty()) {
                    añadirDatosLayout(facebook, R.drawable.facebook_48, linearLayoutPrincipal);
                }
                if (!twitter.isEmpty()) {
                    añadirDatosLayout(twitter, R.drawable.twitter_48, linearLayoutPrincipal);
                }
                if (!instagram.isEmpty()) {
                    añadirDatosLayout(instagram, R.drawable.instagram_48, linearLayoutPrincipal);
                }

            } while (c.moveToNext());

        }

        c.close();

        mapa = new Button(this);

        mapa.setVisibility(View.VISIBLE);

        mapa.setText(R.string.ButtonMaps);

        linearLayoutPrincipal.addView(mapa);

        mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            }
        });


        //Hacemos una consulta para comprobar si el acontecimiento tiene eventos
        Cursor cEvento = db.rawQuery("SELECT * FROM eventos WHERE id_acontecimientos='" + id + "'", null);

        // Contamos el numero de row que devuelve la consulta
        numColumnEvento = cEvento.getCount();


        //Si tiene 0 columnas significa que no tiene eventos por lo que hacemos el boton flotante invisible
        if (numColumnEvento == 0) {
            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
            floatingActionButton.setVisibility(View.INVISIBLE);
        }

        //Boton flotante
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), EventosActivity.class));
            }
        });

    }

    /**
     * @param nombre
     * @param rutaImagen
     * @param layoutparams
     */
    protected void añadirDatosLayout(String nombre, int rutaImagen, LinearLayout layoutparams) {
        //Crea un segundo layout en el que introduciremos los campos
        LinearLayout milayout = new LinearLayout(new ContextThemeWrapper(this, R.style.AppTheme));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        milayout.setLayoutParams(params);
        milayout.setOrientation(LinearLayout.HORIZONTAL);

        //Creamos un nuevo objeto ImageView para asignarle un icono
        ImageView imagen = new ImageView(new ContextThemeWrapper(this, R.style.AppTheme));
        imagen.setImageResource(rutaImagen);

        //Creamos un nuevo objeto TextView para pasarle cada dato obtenido de la consulta
        TextView campo = new TextView(new ContextThemeWrapper(this, R.style.AppTheme));
        campo.setText(nombre);


        //Añadimos el layoutParams al TextView y al ImageView
        imagen.setLayoutParams(params);
        campo.setLayoutParams(params);

        //Añadimos los dos objetos al LinearLayout
        milayout.addView(imagen);
        milayout.addView(campo);

        //Se añade nuestro layout secundario al principal
        layoutparams.addView(milayout);

    }

    /**
     * Logs
     */


    @Override
    protected void onStart() {
        Mylog.d(ACTIVITY, "onStart...");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Mylog.d(ACTIVITY, "onResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Mylog.d(ACTIVITY, "onPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Mylog.d(ACTIVITY, "onStop...");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Mylog.d(ACTIVITY, "onRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Mylog.d(ACTIVITY, "onDestroy...");
        super.onDestroy();
    }
}
