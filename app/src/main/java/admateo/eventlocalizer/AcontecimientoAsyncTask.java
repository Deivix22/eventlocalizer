package admateo.eventlocalizer;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by David on 25/10/2016.
 */

class AcontecimientoAsyncTask extends AsyncTask<String, String, Boolean> {

    //Creamos las variables que usaremos en la clase
    private Context context;
    private View v;
    private ProgressBar progressBar;
    private Boolean comprobarAsyncTask = true;
    private HttpURLConnection urlConnection;
    private String id;
    private Boolean comprobarAcontecimiento = true;

    /**
     * Constructor de la clase que recibe por parametro el contexto, la vista, el id del acontecimiento que pasamos por el textview y la progressBar
     */
    AcontecimientoAsyncTask(Context context, View v, String id, ProgressBar progressBar) {
        super();
        this.context = context;
        this.id = id;
        this.v = v;
        this.progressBar = progressBar;
    }

    /**
     * Metodo que se ejecutara antes de realizar la conexion asincrona, se encarga de hacer visible la progressBar
     */
    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * En este metodo que ejecuta la tarea asincrona, se conecta con el servidor rest y descarga el acontecimiento segun el id pasado, crea la bbdd SQlite y guarda en ella el acontecimiento
     *
     * @param args
     * @return compobarAsyncTask
     */
    protected Boolean doInBackground(String... args) {

        Mylog.d("doInBackgroud", "background");

        StringBuilder result = new StringBuilder();

        try {
            //url de la conexion con el servidor rest
            URL url = new URL("http://eventlocalizer.hol.es/api/v1/acontecimiento/" + id);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            //Crea el objeto para la creacion de la bbdd SQlite
            AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(context, Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
            SQLiteDatabase db = AconSQLite.getWritableDatabase();
            AconSQLite.getReadableDatabase();

            ContentValues registroAcontecimiento = new ContentValues();
            ContentValues registroEventos = new ContentValues();

            JSONObject JsonAcontecimientoRaiz = new JSONObject(result.toString());

            //Si existe acontecimiento comprueba los campos y los inserta en la bbdd
            if (JsonAcontecimientoRaiz.has("acontecimiento")) {
                Mylog.d("tag", "Contiene acontecimiento");

                JSONObject JsonAcontecimientoInterno = new JSONObject(JsonAcontecimientoRaiz.getString("acontecimiento"));

                //Comprobar si los campos estan vacios o no
                String id = (JsonAcontecimientoInterno.has("id") ? JsonAcontecimientoInterno.getString("id") : "");
                String nombre = (JsonAcontecimientoInterno.has("nombre") ? JsonAcontecimientoInterno.getString("nombre") : "");
                String organizador = JsonAcontecimientoInterno.has("organizador") ? JsonAcontecimientoInterno.getString("organizador") : "";
                String descripcion = (JsonAcontecimientoInterno.has("descripcion") ? JsonAcontecimientoInterno.getString("descripcion") : "");
                String tipo = (JsonAcontecimientoInterno.has("tipo") ? JsonAcontecimientoInterno.getString("tipo") : "");
                String portada = (JsonAcontecimientoInterno.has("portada") ? JsonAcontecimientoInterno.getString("portada") : "");
                String inicio = (JsonAcontecimientoInterno.has("inicio") ? JsonAcontecimientoInterno.getString("inicio") : "");
                String fin = (JsonAcontecimientoInterno.has("fin") ? JsonAcontecimientoInterno.getString("fin") : "");
                String direccion = (JsonAcontecimientoInterno.has("direccion") ? JsonAcontecimientoInterno.getString("direccion") : "");
                String localidad = (JsonAcontecimientoInterno.has("localidad") ? JsonAcontecimientoInterno.getString("localidad") : "");
                String cod_postal = (JsonAcontecimientoInterno.has("cod_postal") ? JsonAcontecimientoInterno.getString("cod_postal") : "");
                String provincia = (JsonAcontecimientoInterno.has("provincia") ? JsonAcontecimientoInterno.getString("provincia") : "");
                String longitud = (JsonAcontecimientoInterno.has("longitud") ? JsonAcontecimientoInterno.getString("longitud") : "");
                String latitud = (JsonAcontecimientoInterno.has("latitud") ? JsonAcontecimientoInterno.getString("latitud") : "");
                String telefono = (JsonAcontecimientoInterno.has("telefono") ? JsonAcontecimientoInterno.getString("telefono") : "");
                String email = (JsonAcontecimientoInterno.has("email") ? JsonAcontecimientoInterno.getString("email") : "");
                String web = (JsonAcontecimientoInterno.has("web") ? JsonAcontecimientoInterno.getString("web") : "");
                String facebook = (JsonAcontecimientoInterno.has("facebook") ? JsonAcontecimientoInterno.getString("facebook") : "");
                String twitter = (JsonAcontecimientoInterno.has("twitter") ? JsonAcontecimientoInterno.getString("twitter") : "");
                String instagram = (JsonAcontecimientoInterno.has("instagram") ? JsonAcontecimientoInterno.getString("instagram") : "");

                //Meter los datos en registroAcontecimiento
                registroAcontecimiento.put("id", id);
                registroAcontecimiento.put("nombre", nombre);
                registroAcontecimiento.put("organizador", organizador);
                registroAcontecimiento.put("descripcion", descripcion);
                registroAcontecimiento.put("tipo", tipo);
                registroAcontecimiento.put("portada", portada);
                registroAcontecimiento.put("inicio", inicio);
                registroAcontecimiento.put("fin", fin);
                registroAcontecimiento.put("direccion", direccion);
                registroAcontecimiento.put("localidad", localidad);
                registroAcontecimiento.put("cod_postal", cod_postal);
                registroAcontecimiento.put("provincia", provincia);
                registroAcontecimiento.put("longitud", longitud);
                registroAcontecimiento.put("latitud", latitud);
                registroAcontecimiento.put("telefono", telefono);
                registroAcontecimiento.put("email", email);
                registroAcontecimiento.put("web", web);
                registroAcontecimiento.put("facebook", facebook);
                registroAcontecimiento.put("twitter", twitter);
                registroAcontecimiento.put("instagram", instagram);

                //Insertar los datos
                db.delete("acontecimiento", "id=" + id + "", null);
                db.insert("acontecimiento", null, registroAcontecimiento);

                //Comprueba que exista algun evento y comprueba los campos vacios para luego guardarlo en la bbdd
                if (JsonAcontecimientoRaiz.has("eventos")) {
                    JSONArray listaEventos = new JSONArray(JsonAcontecimientoRaiz.getString("eventos"));

                    for (int i = 0; i < listaEventos.length(); i++) {
                        String idEvento = (listaEventos.getJSONObject(i).has("id") ? listaEventos.getJSONObject(i).getString("id") : "");
                        String idAcontecimiento = (listaEventos.getJSONObject(i).has("id_acontecimientos") ? listaEventos.getJSONObject(i).getString("id_acontecimientos") : "");
                        String nombreEvento = (listaEventos.getJSONObject(i).has("nombre") ? listaEventos.getJSONObject(i).getString("nombre") : "");
                        String descripcionEvento = (listaEventos.getJSONObject(i).has("descripcion") ? listaEventos.getJSONObject(i).getString("descripcion") : "");
                        String inicioEvento = (listaEventos.getJSONObject(i).has("inicio") ? listaEventos.getJSONObject(i).getString("inicio") : "");
                        String finEvento = (listaEventos.getJSONObject(i).has("fin") ? listaEventos.getJSONObject(i).getString("fin") : "");
                        String direccionEvento = (listaEventos.getJSONObject(i).has("direccion") ? listaEventos.getJSONObject(i).getString("direccion") : "");
                        String localidadEvento = (listaEventos.getJSONObject(i).has("localidad") ? listaEventos.getJSONObject(i).getString("localidad") : "");
                        String cod_postalEvento = (listaEventos.getJSONObject(i).has("cod_postal") ? listaEventos.getJSONObject(i).getString("cod_postal") : "");
                        String provinciaEvento = (listaEventos.getJSONObject(i).has("provincia") ? listaEventos.getJSONObject(i).getString("provincia") : "");
                        String longitudEvento = (listaEventos.getJSONObject(i).has("longitud") ? listaEventos.getJSONObject(i).getString("longitud") : "");
                        String latitudEvento = (listaEventos.getJSONObject(i).has("latitud") ? listaEventos.getJSONObject(i).getString("latitud") : "");

                        /******************Guardamos los eventos********************/

                        registroEventos.put("id", idEvento);
                        registroEventos.put("id_acontecimientos", idAcontecimiento);
                        registroEventos.put("nombre", nombreEvento);
                        registroEventos.put("descripcion", descripcionEvento);
                        registroEventos.put("inicio", inicioEvento);
                        registroEventos.put("fin", finEvento);
                        registroEventos.put("direccion", direccionEvento);
                        registroEventos.put("localidad", localidadEvento);
                        registroEventos.put("cod_postal", cod_postalEvento);
                        registroEventos.put("provincia", provinciaEvento);
                        registroEventos.put("longitud", longitudEvento);
                        registroEventos.put("latitud", latitudEvento);

                        /******************Insertamos los eventos********************/

                        db.insert("eventos", null, registroEventos);

                    }
                }
            } else {
                comprobarAcontecimiento = false;
                Snackbar.make(v, R.string.EventDontExist, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } catch (Exception e) {
            Snackbar.make(v, R.string.ErrorServidor, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            e.printStackTrace();
            comprobarAsyncTask = false;
        } finally {
            urlConnection.disconnect();
        }
        return comprobarAsyncTask;
    }

    /**
     * Metodo que se ejecuta una vez terminada la tarea asincrona, pone invisible la progressBar, guarda en un fichero xml el id que hemos del acontecimiento y inicia la activity MostrarAcontecimientoActivity
     *
     * @param result
     */
    @Override
    protected void onPostExecute(Boolean result) {
        //Do something with the JSON string
        progressBar.setVisibility(View.INVISIBLE);
        if (comprobarAsyncTask && comprobarAcontecimiento) {
            SharedPreferences prefs = context.getSharedPreferences("prefenciasID", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("id_acontecimiento", id);
            editor.apply();
            Intent intent = new Intent(context, MostrarAcontecimientoActivity.class);
            context.startActivity(intent);
            ((Activity) context).finish();
        }
    }
}