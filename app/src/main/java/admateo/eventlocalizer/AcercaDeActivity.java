package admateo.eventlocalizer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


/**
 * Created by David on 18/10/2016.
 */

public class AcercaDeActivity extends AppCompatActivity {

    private static final String ACTIVITY = "AcercaDe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_acercade);
        setSupportActionBar(toolbar);

    }

    /*
    * Logs
     */
    @Override
    protected void onStart() {
        Mylog.d(ACTIVITY, "onStart...");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Mylog.d(ACTIVITY, "onResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Mylog.d(ACTIVITY, "onPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Mylog.d(ACTIVITY, "onStop...");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Mylog.d(ACTIVITY, "onRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Mylog.d(ACTIVITY, "onDestroy...");
        super.onDestroy();
    }

}
