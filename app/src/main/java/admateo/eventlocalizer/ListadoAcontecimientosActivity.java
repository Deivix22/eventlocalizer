package admateo.eventlocalizer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by David on 17/01/2017.
 */

public class ListadoAcontecimientosActivity extends AppCompatActivity{

    private static final String ACTIVITY = "ListadoAcontecimientosActivity";
    private ArrayList<AcontecimientoItem> items;
    private Context context = this;

    private String titulo = "";
    private String body = "";

    /**
     * Crea un arrayList en el que se mostraran los acontecimientos, ademas crea un reciclerView
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Este trozo de codigo ejecuta oneSignal y muestra en un Toast el titulo y mensaje de la notificacion Push
        OneSignal.startInit(this).setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
            @Override
            public void notificationOpened(OSNotificationOpenResult result) {
                titulo= result.notification.payload.title;
                body = result.notification.payload.body;

               Toast.makeText(getApplicationContext(),"Titulo "+titulo+ " mensaje: "+body, Toast.LENGTH_LONG).show();
            }
        }).init();

        setContentView(R.layout.activity_listado_acontecimientos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_acercade);
        setSupportActionBar(toolbar);

        //Boton flotante
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AnadirAcontecimientoActivity.class));
            }
        });

        }

    /**
     * Crea el menu de opciones
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listado_acontecimientos, menu);
        return true;
    }

    /**
     * En este metodo definimos lo que queremos que haga el menu superior, en este caso sera iniciar la actividad AcercaDeActivity
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Recoge el id de la opcion del menu que hemos pulsado
        int id = item.getItemId();

        //Hace una comprobacion, si el id que hemos pulsado es igual al id de la activity se ejecuta el codigo
        switch (id) {
            case R.id.AcercaDe:
                startActivity(new Intent(getApplicationContext(), AcercaDeActivity.class));
                break;
            case R.id.Configuracion:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Logs
     */

    @Override
    protected void onStart() {
        Mylog.d(ACTIVITY, "onStart...");
        super.onStart();

        AcontecimientoSQLite AconSQLite = new AcontecimientoSQLite(this, Environment.getExternalStorageDirectory() + "/Acontecimientos.db", null, 1);
        SQLiteDatabase db = AconSQLite.getReadableDatabase();


        //Creamos el cursor y hacemos la consulta select que nos devolvera el acontecimiento
        final Cursor c = db.rawQuery("SELECT id, nombre, inicio, fin FROM acontecimiento ORDER BY inicio desc", null);

        if (c.moveToFirst()) {
            TextView texto = (TextView) findViewById(R.id.textView_sin_acontecimientos);
            texto.setVisibility(View.GONE);

            // Crear elementos
            items = new ArrayList<>();

            //Creamos los formatos de entrada y de salida de las fechas
            SimpleDateFormat formatoDeEntrada = new SimpleDateFormat("yyyyMMddHHmm");
            SimpleDateFormat formatoDeSalida = new SimpleDateFormat("dd/MM/yyyy");

            //Creamos las dos variables de tipo date y las inicializamos a null porque deben inicializarse
            Date dateInicio = null;
            Date dateFin = null;

            do {
                try {
                    dateInicio = formatoDeEntrada.parse("" + c.getString(c.getColumnIndex("inicio")) + "");
                    dateFin = formatoDeEntrada.parse("" + c.getString(c.getColumnIndex("fin")) + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String inicio = formatoDeSalida.format(dateInicio);
                String fin = formatoDeSalida.format(dateFin);

                //Rellenar los arraylist con los elementos de la bbdd
                items.add(new AcontecimientoItem("" + c.getString(c.getColumnIndex("id")) + "", "" + c.getString(c.getColumnIndex("nombre")) + "", "" + inicio + "" + " - ", "" + fin + ""));

            } while (c.moveToNext());

            // Se inicializa el RecyclerView
            final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

            // Se crea el Adaptador con los datos
            AcontecimientoAdapter adaptador = new AcontecimientoAdapter(items);

            // Se asocia el elemento con una acción al pulsar el elemento
            adaptador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Acción al pulsar el elemento
                    Mylog.d(ACTIVITY, "Click en RecyclerView");
                    int position = recyclerView.getChildAdapterPosition(v);


                    //añadimos al fichero XML el id del acontecimiento seleccionado
                    SharedPreferences prefs = context.getSharedPreferences("prefenciasID", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("id_acontecimiento", items.get(position).getId());
                    editor.apply();

                    //lanzamos la activity mostrar acontecimiento que mostrara los datos de el acontecimiento que hemos pulsado
                    startActivity(new Intent(getApplicationContext(), MostrarAcontecimientoActivity.class));

                }
            });

            // Se asocia el Adaptador al RecyclerView
            recyclerView.setAdapter(adaptador);

            // Se muestra el RecyclerView en vertical
            recyclerView.setLayoutManager(new LinearLayoutManager(this));


        } else {

            TextView texto = (TextView) findViewById(R.id.textView_sin_acontecimientos);
            texto.setVisibility(View.VISIBLE);
        }


    }

    @Override
    protected void onResume() {
        Mylog.d(ACTIVITY, "onResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Mylog.d(ACTIVITY, "onPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Mylog.d(ACTIVITY, "onStop...");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Mylog.d(ACTIVITY, "onRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Mylog.d(ACTIVITY, "onDestroy...");
        super.onDestroy();
    }

    @Override
    public void onBackPressed(){
        finish();
    }
}
